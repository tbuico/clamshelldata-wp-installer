#!/bin/bash

# Clamshell Data, https://clamshelldata.com
# This script uses WP-Quick-Install by GeekPress, the github master repo can be found here: https://github.com/GeekPress/WP-Quick-Install

printf "%s\n""\e[0;32m" && echo  "Starting automatic installation of Wordpress, please be patient." && printf "\e[0m\n"

# shellcheck disable=2034,2059
true
# shellcheck source=lib.sh
FIRST_IFACE=1 && CHECK_CURRENT_REPO=1 . <(curl -sL https://gitlab.com/hungrykanamit/Wordpress/raw/master/Wordpress-files/lib.sh)
unset FIRST_IFACE
unset CHECK_CURRENT_REPO

# Download WP-Quick-Install
printf "%s\n""\e[0;32m" && echo "Downloading WP-Quick-Install by GeekPress" && printf "\e[0m\n"
wget "https://gitlab.com/hungrykanamit/Wordpress/repository/archive.zip" -P "${HTML}"

# Extract package

unzip "$HTML/archive.zip" -d $HTML & spinner_loading
mv $HTML/Wordpress-master-*/clamshelldata-wp-installer/ $HTML/html
mv $HTML/html/index.html $HTML/html/index.html.old
rm -rf $HTML/Wordpress-master-* $HTML/archive.zip

# Secure permissions
printf "%s\n""\e[0;32m" && echo "Securing permissions" && printf "\e[0m\n"
#chmod 750 $HTML/index.php && chown www-data:www-data $HTML/index.php
chmod 750 $HTML/html/clamshelldata-wp-installer/index.php && chown www-data:www-data $HTML/html/clamshelldata-wp-installer/index.php

# Change 000-default to $WEB_ROOT
sudo chown -R www-data:www-data /var/www/
sudo chmod -R 755 /var/www/

# Create WordPress Database
#printf "%s\n""\e[0;32m" && echo "Creating WordPress Database" && printf "\e[0m\n"
#mysql -u root -p$MYSQL_PASS2 -e "CREATE DATABASE $WPDB;"
#mysql -u root -p$MYSQL_PASS2 -e "GRANT ALL PRIVILEGES ON $WPDB.* TO '$WPUSER'@'localhost' IDENTIFIED BY '$WPPASS';"
#mysql -u root -p$MYSQL_PASS2 -e "FLUSH PRIVILEGES;"

# Restart the web server and mysql service
sudo systemctl restart apache2.service
sudo systemctl restart mysql.service

# Upgrade
apt update -q4 & spinner_loading
apt dist-upgrade -y

# Cleanup
printf "%s\n""\e[0;32m" && echo "Cleaning Up" && printf "\e[0m\n"
#clearboot should be used after a reboot, if used in this script (before reboot) then it will delete the newest kernal
#CLEARBOOT=$(dpkg -l linux-* | awk '/^ii/{ print $2}' | grep -v -e ''"$(uname -r | cut -f1,2 -d"-")"'' | grep -e '[0-9]' | xargs sudo apt -y purge)
#echo "$CLEARBOOT"
apt autoremove -y
apt autoclean -y
rm -rf /var/cache/apt/archives/*.deb
find /root "/home/$UNIXUSER" -type f \( -name '*.sh*' -o -name '*.html*' -o -name '*.tar*' -o -name '*.zip*' \) -delete

# Inform about next step
ADDRESS3=$(hostname -I | cut -d ' ' -f 1)
echo
printf "\e[32mSetup finished. Go to \e[0;36mhttp://$ADDRESS3/clamshelldata-wp-installer/index.php\e[32m to install wordpress\e[0m\n"
echo
printf "%s\n""\e[0;32m" && echo "System will Reboot in 10 seconds so a template can be made..." && printf "\e[0m\n" && sleep 10
reboot
