#!/bin/bash

# Clamshell Data, https://clamshelldata.com
# This script uses WP-Quick-Install by GeekPress, the github master repo can be found here: https://github.com/GeekPress/WP-Quick-Install

printf "%s\n""\e[0;32m" && echo  "Starting automatic installation of Wordpress, please be patient." && printf "\e[0m\n"

# Prefer IPv4
sed -i "s|#precedence ::ffff:0:0/96  100|precedence ::ffff:0:0/96  100|g" /etc/gai.conf

# shellcheck disable=2034,2059
true
# shellcheck source=lib.sh
FIRST_IFACE=1 && CHECK_CURRENT_REPO=1 . <(curl -sL https://gitlab.com/hungrykanamit/Wordpress/raw/master/Wordpress-files/lib.sh)
unset FIRST_IFACE
unset CHECK_CURRENT_REPO

# Upgrade
printf "%s\n""\e[0;32m" && echo "Upgrading OS" && printf "\e[0m\n"
apt update -q4 & spinner_loading
apt dist-upgrade -y

# Cleanup
printf "%s\n""\e[0;32m" && echo "Cleaning Up After Upgrades" && printf "\e[0m\n"
apt autoremove -y
apt autoclean -y
rm -rf /var/cache/apt/archives/*.deb

# Install various packages needed for the Scripts
apt install -y unzip #expect

# Install MYSQL 5.7
printf "%s\n""\e[0;32m" && echo "Installing MySQL" && printf "\e[0m\n"
apt install software-properties-common -y
apt install mysql-client -y
echo "mysql-server-5.7 mysql-server/root_password password $MYSQL_PASS2" | debconf-set-selections
echo "mysql-server-5.7 mysql-server/root_password_again password $MYSQL_PASS2" | debconf-set-selections
apt install mysql-server-5.7 -y

# mysql_secure_installation
apt -y install expect
SECURE_MYSQL=$(expect -c "
set timeout 10
spawn mysql_secure_installation
expect \"Enter current password for root:\"
send \"$MYSQL_PASS2\r\"
expect \"Would you like to setup VALIDATE PASSWORD plugin?\"
send \"n\r\"
expect \"Change the password for root ?\"
send \"n\r\"
expect \"Remove anonymous users?\"
send \"y\r\"
expect \"Disallow root login remotely?\"
send \"y\r\"
expect \"Remove test database and access to it?\"
send \"y\r\"
expect \"Reload privilege tables now?\"
send \"y\r\"
expect eof
")
echo "$SECURE_MYSQL"
apt -y purge expect

#SLIMClEAN
apt autoremove -y
apt autoclean -y
rm -rf /var/cache/apt/archives/*.deb

# Install Apache
printf "%s\n""\e[0;32m" && echo "Installing Apache" && printf "\e[0m\n"
check_command apt install apache2 apache2-utils -y
a2enmod rewrite \
        headers \
        env \
        dir \
        mime \
        ssl \
        setenvif
systemctl enable apache2
systemctl start apache2

#SLIMClEAN
apt autoremove -y
apt autoclean -y
rm -rf /var/cache/apt/archives/*.deb

# Install PHP 7.0
printf "%s\n""\e[0;32m" && echo "Installing PHP" && printf "\e[0m\n"
apt update -q4 & spinner_loading
check_command apt install -y \
    php7.0-mysql  \
    libapache2-mod-php7.0  \
    php7.0-cli  \
    php7.0-cgi  \
    php7.0-zip  \
    php7.0-gd

## Make a PHPinfo page
printf "%s\n""\e[0;32m" && echo "Generate PHPinfo page" && printf "\e[0m\n"
cat > /var/www/html/phpinfo.php << EOF
<?php
//Show all information, defaults to INFO_ALL
phpinfo();
?>
EOF

#SLIMClEAN
apt autoremove -y
apt autoclean -y
rm -rf /var/cache/apt/archives/*.deb

# Upgrade
apt update -q4 & spinner_loading
apt dist-upgrade -y

# Remove LXD (always shows up as failed during boot)
apt purge lxd -y

# Cleanup
printf "%s\n""\e[0;32m" && echo "Cleaning Up" && printf "\e[0m\n"
apt autoremove -y
apt autoclean -y
rm -rf /var/cache/apt/archives/*.deb
find /root "/home/$UNIXUSER" -type f \( -name '*.sh*' -o -name '*.html*' -o -name '*.tar*' -o -name '*.zip*' \) -delete

reboot
