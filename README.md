clamshelldata-wp-installer
================

clamshelldata-wp-installer is based on version 1.4.2 of WP Quick Install by GeekPress (Jonathan Buttigieg & Julio Potier) for more information on the Original script go to the following page: http://wp-quick-install.com

clamshelldata-wp-installer is an easy way to install WordPress while giving users full control of the installation process including MySQL connection details. It is a lightweight script which automatically downloads and installs WordPress, and any plugins or themes you want.

This was written to help deploy WordPress cloud servers to clients of Clamshell Data, http://clamshelldata.com

The only requirement of this script is to have a fresh installation of Ubuntu 16.04 x64 or newer.

## Installation Process

1. Download the .zip archive to your newly deployed Ubuntu server, unzip the contents and run the pre-install script
```
bash Wordpress/Wordpress-files/ubuntu16_clamshelldata-wp-installer_pre-install.sh
```

2. After the automated reboot, run the provisioner-only script
```
bash Wordpress/Wordpress-files/ubuntu16_clamshelldata-wp-installer_provisioner-only.sh
```

3. After the second automated reboot, you can now just enter in your ip address into the url bar of a web browser and navigate to the clamshelldata-wp-installer page.

4. Follow the instructions on the clamshelldata-wp-installer page.

Changelog
================
1.5.2
-----------
* Updated original WP Quick Install script so that MySQL connection details can be created directly from the WordPress install page.
* Fixed spelling and grammar errors
* Added install scripts for Ubuntu 16.04 x64 or newer servers so that all dependencies are met and script will run smoothly in production environments.
* Changed a lot of other minor details intended for Clamshell Data Clients.

1.4.2
-----------
* Delete Tweentyfifteen & Tweentysixteen themes

1.4.1
-----------
* Fix quote issue with WordPress.com API Key

1.4
-----------
* Fix database issue since WordPress 4.1
* You can add your WordPress.com API Key

1.3.3
-----------

* Add SSL compatibility
* Remove SSL function (cause trouble with process installation)

1.3.2
-----------

* Add a script header
* Security improvement

1.3.1
-----------

* Fix error for PHP > 5.5: Strict standards: Only variables should be passed by reference in ../wp-quick-install/index.php on line 10

1.3
-----------

* Possiblity to select WordPress language installation
* Permaling management


1.2.8.1
-----------

* You can now declare articles to be generated via data.ini file
* Fix bug on new articles
* You can now select the revision by articles

1.2.8
-----------

* Media management

1.2.7.2
-----------

* Security : Forbiden access to data.ini from the browser

1.2.7.1
-----------

* noindex nofollow tag.

1.2.7
-----------

* Premium extension by adding archives in plugins folder
* You can enable extension after installation
* Auto supression of Hello Dolly extension
* You can add a theme and enable it
* You can delete Twenty Elever and Twenty Ten

1.2.6
-----------

* Fix a JS bug with data.ini

1.2.5
-----------

* You can delete the default content added by WordPress
* You can add new pages with data.ini
* Data.ini update

1.2.4
-----------

* Two new debug options : *Display errors* (WP_DEBUG_DISPLAY) and *Write errors in a log file* (WP_DEBUG_LOG)

1.2.3
-----------

* SEO Fix bug
* Automatic deletion of licence.txt and readme.html

1.2.2
-----------

* Deletion of all exec() fucntions
* Unzip WordPress and plugins with ZipArchive class
* Using scandir() and rename() to move WordPress files

1.2.1
-----------

* Checking chmod on parent folder
* Adding a link to website and admin if success

1.2
-----------

* You can now pre-configure the form with data.ini


1.1
-----------

* Code Optimisation


1.0
-----------

* Initial Commit
